library(ggplot2)

fixed_cost_per_batch = 0.55 #F
fixed_cost_per_item = 0.19 #V
total_items_demand = 100 #N
total_holding_cost_as_percent = 18.54 #H

# optimise
optimal_batch_size <- sqrt(
    (2 * fixed_cost_per_batch * total_items_demand) /
    (fixed_cost_per_item*total_holding_cost_as_percent)
)

transaction_cost = function(f, n, q) { (f * n) / q }
holding_cost = function(q, v, h) { (q * v * h) / 2 }

batch_sizes <- seq(1, 10)
transaction_costs <- NULL
holding_costs <- NULL
total_costs <- NULL

for(q in batch_sizes) {
  transaction <- transaction_cost(fixed_cost_per_batch, total_items_demand, q)
  holding <- holding_cost(q, fixed_cost_per_item, total_holding_cost_as_percent)
  
  transaction_costs <- c(transaction_costs, transaction)
  holding_costs <- c(holding_costs, holding)
  total_costs <- c(total_costs, (transaction + holding))
}

# plot
costs = data.frame(transaction = transaction_costs, holding = holding_costs, total = total_costs)

ggplot(costs, aes(batch_sizes)) +
  geom_path(aes(y = transaction, colour = "transaction")) +
  geom_path(aes(y = holding, colour = "holding")) +
  geom_path(aes(y = total, colour = "total")) +
  geom_vline(xintercept = optimal_batch_size)
  
