```
# install gems
sudo gem install serverspec docker-api

# test container
rspec test/spec.rb --format documentation

# build container
docker build -t tdd-infrastructure-ruby .
docker run -it tdd-infrastructure-ruby /bin/bash
```
