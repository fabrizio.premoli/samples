require "serverspec"
require "docker-api"

describe "Dockerfile" do
  before(:all) do
    @container = Docker::Image.build_from_dir('.')
    set :docker_image, @container.id

    set :backend, :docker
    set :os, family: :debian
  end
  
  it "is Debian (stretch)" do
    expect(os_family).to include("Debian GNU/Linux 9")
  end

  it "with redis-server installed" do
    expect(package("redis-server")).to be_installed
  end

  it "and is possible to PING it" do
    expect(redis_server_ping).to include("PONG")
  end

  def os_family
    command("cat /etc/issue.net").stdout
  end

  def redis_server_ping
    command("service redis-server start && redis-cli PING").stdout
  end
end